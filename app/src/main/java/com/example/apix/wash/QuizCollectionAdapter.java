package com.example.apix.wash;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

/**
 * Created by Apix on 10/09/2017.
 */

public class QuizCollectionAdapter extends BaseAdapter {

    private ArrayList<QuizCollection> mData;
    private Context mContext;
    private String[] ans_questions;

    private static String TAG = QuizCollectionAdapter.class.getSimpleName();

    RequestOptions requestOptions = new RequestOptions()
            .placeholder(R.drawable.ic_image)
            .error(R.drawable.ic_error)
            .fallback(R.drawable.ic_broken_image);

    public QuizCollectionAdapter(Context context,ArrayList<QuizCollection> data,String ans_ques) {
        mContext = context;
        mData = data;
        ans_questions = ans_ques.split(" ");
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int pos) {
        return mData.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.quiz_collection, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.title = rowView.findViewById(R.id.title);
            viewHolder.desc = rowView.findViewById(R.id.desc);
            viewHolder.image = rowView.findViewById(R.id.image);
            viewHolder.status = rowView.findViewById(R.id.status);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

//        GlideApp.with(mContext).load("http://nabadilika.dodoma-antinetal-pns.or.tz/img/"+mData.get(position).img_url).apply(requestOptions).into(holder.image);
        GlideApp.with(mContext).load("http://192.168.43.125/wash/images/"+mData.get(position).img_url).apply(requestOptions).into(holder.image);
        holder.title.setText(mData.get(position).title);
        holder.desc.setText(mData.get(position).description);
        for(int i=0;i<ans_questions.length;i++){
            if(ans_questions[i].equals(String.valueOf(mData.get(position).id))){
                holder.status.setText("Done");
            }
        }

        return rowView;
    }


    static class ViewHolder {
        public TextView title,desc,status;
        public ImageView image;
    }
}