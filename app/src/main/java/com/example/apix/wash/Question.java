package com.example.apix.wash;

import java.util.ArrayList;

/**
 * Created by Apix on 10/09/2017.
 */

public class Question {
    int qn_no;
    String question,fact;
    ArrayList<Answer> answers;

    public Question(int qn_no, String question,String fact, ArrayList<Answer> answers) {
        this.qn_no = qn_no;
        this.question = question;
        this.fact = fact;
        this.answers = answers;
    }
}
