package com.example.apix.wash;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class AnsweringFragment extends Fragment implements SelectedPositionInterface{

    Button btn_next;
    TextView question,qn_txt,quiz_title;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    RecyclerView.LayoutManager layoutManager;
    AnswerAdapter adapter;

    ArrayList<Question> questions;

    int counter = 0;
    String choice = "none";
    int correctness = 0;
    int win = 0;
    int score;

    int user_coins;
    String user_id;
    String name;
    String img_url;

//    private static String questions_url = "http://nabadilika.dodoma-antinetal-pns.or.tz/wash/data_json.php";
    private static String questions_url = "http://192.168.43.125/wash/data_json.php";

//    String quiz_answered_url = "http://nabadilika.dodoma-antinetal-pns.or.tz/wash/quiz_answered_update.php";
    String quiz_answered_url = "http://192.168.43.125/wash/quiz_answered_update.php";

    private static String TAG = AnsweringFragment.class.getSimpleName();

    public AnsweringFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_answering, container, false);

        Bundle arguments = getArguments();
        String quiz_header = arguments.getString("quiz_header");
        final String quiz_id = arguments.getString("quiz_id");
        user_coins = arguments.getInt("user_points");
        user_id = arguments.getString("user_id");
        name = arguments.getString("name");
        img_url = arguments.getString("img_url");

        btn_next = view.findViewById(R.id.btn_next);
        question = view.findViewById(R.id.question);
        qn_txt = view.findViewById(R.id.qn_txt);
        quiz_title = view.findViewById(R.id.quiz_title);
        recyclerView = view.findViewById(R.id.recyclerView);
        progressBar = view.findViewById(R.id.progressBar);

        quiz_title.setText(quiz_header);

        progressBar.getProgressDrawable().setColorFilter(
                getResources().getColor(R.color.progress_bar), android.graphics.PorterDuff.Mode.SRC_IN);

        layoutManager = new LinearLayoutManager(getContext());

        new ResultsTask(this).execute(quiz_id);

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(counter<questions.size()){
                    if(choice.equals("yes")){
                        if(correctness==1){
                            //win++;
                            win = win + score;
                            new BottomDialog.Builder(getContext())
                                    .setTitle("Awesome!")
                                    .setContent(questions.get(counter).fact)
                                    .setIcon(R.drawable.rate4)
                                    //.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_launcher))
                                    .show();
                        }else{
                            win = win + score;
                            new BottomDialog.Builder(getContext())
                                    .setTitle("Oops!")
                                    .setContent(questions.get(counter).fact)
                                    .setIcon(R.drawable.rate3)
                                    //.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_launcher))
                                    .show();
                        }
                        int count = counter;
                        count++;
                        progressBar.setProgress(count);
                        qn_txt.setText("Question "+count+"/"+questions.size());
                        adapter.clear();
                        addNewAnswers();
                        question.setText(questions.get(counter).question);
                        counter++;
                    }else{
                        Toast.makeText(getContext(),"Please choose to continue..",Toast.LENGTH_SHORT).show();
                    }

                }else{
//                    if(correctness==1){
//                        win++;
//                    }
//                    if(isOnline()){
                        win = win + score;
                        String result = win+" "+questions.size();
                        new QuizAsweredTask().execute(user_id,quiz_id,String.valueOf(win));
                        Intent intent = new Intent(getContext(),ResultsActivity.class);
                        intent.putExtra("result",result);
                        intent.putExtra("user_id",user_id);
                        intent.putExtra("name",name);
                        intent.putExtra("img_url",img_url);
                        intent.putExtra("user_points",String.valueOf(user_coins));
                        startActivity(intent);
                        getActivity().finish();
//                    }else{
//                        Toast.makeText(getContext(), "Check Connection..", Toast.LENGTH_SHORT).show();
//                    }

                }
            }
        });

        return view;
    }

    public void addNewAnswers(){
        adapter = new AnswerAdapter(getContext(),this,questions.get(counter).answers);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AnswerAdapter.AnswerViewHolder holder, int correctness, String choice, int score) {
        this.choice = choice;
        this.correctness = correctness;
        this.score = score;
    }

    private class ResultsTask extends AsyncTask<String, Void, String> {

        SelectedPositionInterface selectInterface;

        public ResultsTask(SelectedPositionInterface selectInterface) {
            this.selectInterface = selectInterface;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            String quiz_id = params[0];
            Log.d(TAG, "hapa: "+quiz_id);

            try {
                URL url = new URL(questions_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("quiz_id","UTF-8")+"="+URLEncoder.encode(quiz_id,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String response = "";
                String line;
                while ((line = bufferedReader.readLine())!=null)
                {
                    response+= line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

////            candidatesList = new ArrayList<>();

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            questions = new ArrayList<>();

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("questions");

                Log.d(TAG, "doInBackground: "+jsonArray);

                for(int i=0;i<jsonArray.length();i++){
                    JSONObject jsonQuestion = jsonArray.getJSONObject(i);

                    int qn_id = jsonQuestion.getInt("qn_id");
                    String question = jsonQuestion.getString("question");
                    String fact = jsonQuestion.getString("fact");


                    ArrayList<Answer> answers = new ArrayList<>();

                    JSONArray jsonArray1 = jsonQuestion.getJSONArray("answers");
                    for(int j=0;j<jsonArray1.length();j++){
                        JSONObject jsonAnswer = jsonArray1.getJSONObject(j);

                        int is_correct = jsonAnswer.getInt("is_correct");
                        String answer = jsonAnswer.getString("answer");
                        int score = Integer.parseInt(jsonAnswer.getString("score"));

                        answers.add(new Answer(is_correct,answer,score));
                    }

                    questions.add(new Question(qn_id,question,fact,answers));
                }

                Gson gson = new Gson();
                String que = gson.toJson(questions);
                Log.d(TAG, "doInBackground later: "+que);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            progressBar.setMax(questions.size());
            question.setText(questions.get(0).question);
            counter++;
            qn_txt.setText("Question "+counter+"/"+questions.size());
            adapter = new AnswerAdapter(getContext(),selectInterface,questions.get(0).answers);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
        }
    }

    private class QuizAsweredTask extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            String user_id = params[0];
            String quiz_id = params[1];
            String points = params[2];

            try {
                URL url = new URL(quiz_answered_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("user_id","UTF-8")+"="+URLEncoder.encode(user_id,"UTF-8")+"&"+
                        URLEncoder.encode("quiz_id","UTF-8")+"="+URLEncoder.encode(quiz_id,"UTF-8")+"&"+
                        URLEncoder.encode("points","UTF-8")+"="+URLEncoder.encode(points,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String response = "";
                String line;
                while ((line = bufferedReader.readLine())!=null)
                {
                    response+= line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

////            candidatesList = new ArrayList<>();

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            user_coins = Integer.parseInt(result);
            Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
        }
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

}
