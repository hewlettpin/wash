package com.example.apix.wash;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import static java.security.AccessController.getContext;

public class FeedbackActivity extends AppCompatActivity {

    String user_id;
    int user_points;
    EditText feedback_edit_text;
    Button submit_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_feedback);
        changeStatusBarColor();

        user_id = getIntent().getStringExtra("user_id");
        user_points = getIntent().getIntExtra("user_points",0);

        feedback_edit_text = findViewById(R.id.feedback_edit_text);

        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String feedBack_msg = feedback_edit_text.getText().toString();
                if(feedBack_msg.isEmpty()){
                    Toast.makeText(FeedbackActivity.this, "Feedback Field Is Empty!", Toast.LENGTH_SHORT).show();
                }else {
                    if(isOnline()){
                        new FeedbackTask().execute(feedBack_msg,user_id);
                    }else{
                        Toast.makeText(FeedbackActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                    }
                }
                onBackPressed();
            }
        });
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public class FeedbackTask extends AsyncTask<String, Void,String> {

        private String feedback_url = "http://nabadilika.dodoma-antinetal-pns.or.tz/wash/feedback.php";
//        private String feedback_url = "http://192.168.10.100/wash/feedback.php";

        @Override
        protected String doInBackground(String... params) {

            String feedback = params[0];
            String user_id = params[1];
            try {
                URL url = new URL(feedback_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("feedback","UTF-8")+"="+URLEncoder.encode(feedback,"UTF-8")+"&"+
                        URLEncoder.encode("user_id","UTF-8")+"="+URLEncoder.encode(user_id,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String response = "";
                String line;
                while ((line = bufferedReader.readLine())!=null)
                {
                    response+= line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.contains("Successful")) {
                feedback_edit_text.setText("");
                Toast.makeText(getApplicationContext(), "Your Feedback Was Successful Sent!", Toast.LENGTH_LONG).show();
            } else{
                Toast.makeText(getApplicationContext(), "Oops! Something Went Wrong..", Toast.LENGTH_LONG).show();
            }
        }
    }

    public class CoinsTask extends AsyncTask<String, Void,String> {

        private String feedback_url = "http://nabadilika.dodoma-antinetal-pns.or.tz/wash/coin_earn.php";
//        private String feedback_url = "http://192.168.10.100/wash/coin_earn.php";

        @Override
        protected String doInBackground(String... params) {

            String coins = params[0];
            String user_id = params[1];
            try {
                URL url = new URL(feedback_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("coins","UTF-8")+"="+URLEncoder.encode(coins,"UTF-8")+"&"+
                        URLEncoder.encode("user_id","UTF-8")+"="+URLEncoder.encode(user_id,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String response = "";
                String line;
                while ((line = bufferedReader.readLine())!=null)
                {
                    response+= line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("hii", "onPostExecute: "+result);
            if (result.contains("Successful")) {
                String[] jibu = result.split("/");
                user_points = Integer.parseInt(jibu[1]);
//                coins_text.setText("You Have "+String.format("%,d", user_points)+" Coins");
                Toast.makeText(getApplicationContext(), jibu[0], Toast.LENGTH_LONG).show();
            } else{
                Toast.makeText(getApplicationContext(), "Oops! Something Went Wrong..", Toast.LENGTH_LONG).show();
            }
        }
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
}
