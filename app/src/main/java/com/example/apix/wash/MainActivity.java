package com.example.apix.wash;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.yalantis.guillotine.animation.GuillotineAnimation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements QuizFragment.MainInterface {

    private static final long RIPPLE_DURATION = 250;


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.root)
    FrameLayout root;
    @BindView(R.id.content_hamburger)
    View contentHamburger;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    NavigationView navigationView;
    TextView nav_header_name;
    TextView nav_header_coins;
    ImageView img;

    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_QUIZ = "quiz";
    private static final String TAG_NOTIFICATION = "notification";
    private static final String TAG_LEADERBOARD = "leaderboard";
    private static final String TAG_COINS = "coins";
    public static String CURRENT_TAG = TAG_QUIZ;

    private static String TAG = MainActivity.class.getSimpleName();

    Handler mHandler;

    private String[] activityTitles;

    QuizFragment quizFragment;
    NotificationFragment notificationFragment;
    LeaderBoardFragment leaderBoardFragment;
    CoinsFragment coinsFragment;
    TextView notification_count;

    GuillotineAnimation guillotineAnimation;

    int points;
    String user_id;
    String name;
    String img_url;

    RequestOptions requestOptions = new RequestOptions()
            .placeholder(R.drawable.prof)
            .error(R.drawable.ic_error)
            .fallback(R.drawable.prof);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (Build.VERSION.SDK_INT >= 21) {
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//        }

        setContentView(R.layout.activity_main);

//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.example.apix.wash",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }

        name = getIntent().getStringExtra("name");
        points = Integer.parseInt(getIntent().getStringExtra("coins"));
        user_id = getIntent().getStringExtra("user_id");
        img_url = getIntent().getStringExtra("img_url");

        ButterKnife.bind(this);

        Typeface typeface = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Bold.ttf");
        toolbar_title.setTypeface(typeface);

        mHandler = new Handler();

        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(null);
        }

        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.top_menu, null);
        root.addView(guillotineMenu);

        navigationView = guillotineMenu.findViewById(R.id.nav_view);

        nav_header_name = navigationView.getHeaderView(0).findViewById(R.id.name);
        nav_header_coins = navigationView.getHeaderView(0).findViewById(R.id.coins);
        img = navigationView.getHeaderView(0).findViewById(R.id.prof);
        nav_header_name.setText(name);
        nav_header_coins.setText(String.format("%,d", points)+" Coins");

        if(img_url!=null){
            GlideApp.with(getApplicationContext()).load("http://nabadilika.dodoma-antinetal-pns.or.tz/wash/images/prof_images/"+img_url).apply(requestOptions).into(img);
        }

//        changeStatusBarColor();

        setUpNavigationView();

        guillotineMenu.findViewById(R.id.btn_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),WelcomeActivity.class));
                finish();
            }
        });

        guillotineAnimation =  new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();

        new NotificationTask().execute(user_id);

        String intentFragment = getIntent().getStringExtra("frgToLoad");
        if(intentFragment==null){
            navItemIndex = 0;
            CURRENT_TAG = TAG_QUIZ;
            loadHomeFragment();
        }else {
            switch (intentFragment){
                case TAG_QUIZ:
                    navItemIndex = 0;
                    CURRENT_TAG = TAG_QUIZ;
                    loadHomeFragment();
                    break;
                case TAG_NOTIFICATION:
                    navItemIndex = 1;
                    CURRENT_TAG = TAG_NOTIFICATION;
                    loadHomeFragment();
                    break;
                case TAG_LEADERBOARD:
                    navItemIndex = 2;
                    CURRENT_TAG = TAG_LEADERBOARD;
                    loadHomeFragment();
                    break;
                case TAG_COINS:
                    navItemIndex = 4;
                    CURRENT_TAG = TAG_COINS;
                    loadHomeFragment();
                    break;
            }
        }
    }

    private void setUpNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()){
                    case R.id.nav_quiz:
                        guillotineAnimation.close();
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_QUIZ;
                        break;
                    case R.id.nav_notification:
                        guillotineAnimation.close();
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_NOTIFICATION;
                        break;
                    case R.id.nav_leaderboard:
                        guillotineAnimation.close();
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_LEADERBOARD;
                        break;
                    case R.id.nav_settings:
                        Intent intent = new Intent(MainActivity.this, SettingsPrefActivity.class);
                        intent.putExtra("frgToLoad",CURRENT_TAG);
                        intent.putExtra("coins",points);
                        intent.putExtra("user_id",user_id);
//                        Log.d(TAG, "onNavigationItemSelected: "+points);
                        startActivity(intent);
                        return true;
                    case R.id.nav_coins:
                        guillotineAnimation.close();
                        navItemIndex = 4;
                        CURRENT_TAG = TAG_COINS;
                        break;
                }
                if (item.isChecked()) {
                    item.setChecked(false);
                } else {
                    item.setChecked(true);
                }
                item.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });
    }

    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar toolbar_title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
//        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
//            drawer.closeDrawers();
//
//            return;
//        }

        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                if(fragment == quizFragment){
                    QuizFragment quizFragment = (QuizFragment) fragment;
                    Bundle arguments = new Bundle();
                    arguments.putString("user_id",user_id);
                    quizFragment.setArguments(arguments);
                    fragmentTransaction.replace(R.id.content_frame,quizFragment,CURRENT_TAG);
                }else if (fragment == notificationFragment){
                    //Remove from other fragments
                    NotificationFragment notificationFragment = (NotificationFragment) fragment;
                    Bundle arguments = new Bundle();
                    arguments.putString("user_id",user_id);
                    notificationFragment.setArguments(arguments);
                    fragmentTransaction.replace(R.id.content_frame, notificationFragment, CURRENT_TAG);
                }else if (fragment == leaderBoardFragment ){
                    //Remove from other fragments
                    LeaderBoardFragment leaderBoardFragment = (LeaderBoardFragment) fragment;
                    Bundle arguments = new Bundle();
                    arguments.putString("user_id",user_id);
                    leaderBoardFragment.setArguments(arguments);
                    fragmentTransaction.replace(R.id.content_frame, leaderBoardFragment, CURRENT_TAG);
                }else if (fragment == coinsFragment ){
                    //Remove from other fragments
                    CoinsFragment coinsFragment = (CoinsFragment) fragment;
                    Bundle arguments = new Bundle();
                    arguments.putInt("user_points",points);
                    arguments.putString("user_id",user_id);
                    coinsFragment.setArguments(arguments);
                    fragmentTransaction.replace(R.id.content_frame, coinsFragment, CURRENT_TAG);
                }
                else {
                    fragmentTransaction.replace(R.id.content_frame, fragment, CURRENT_TAG);
                }
                fragmentTransaction.replace(R.id.content_frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        //Closing drawer on item click
//        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setToolbarTitle() {
        toolbar_title.setText(activityTitles[navItemIndex]);
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // quiz
                quizFragment = new QuizFragment();
                return quizFragment;
            case 1:
                // notification
                notificationFragment = new NotificationFragment();
                return notificationFragment;
            case 2:
                // leaderboard
                leaderBoardFragment = new LeaderBoardFragment();
                return leaderBoardFragment;
            case 4:
                // coin
                coinsFragment = new CoinsFragment();
                return coinsFragment;
            default:
                quizFragment = new QuizFragment();
                return quizFragment;
        }
    }

    @Override
    public void onClick(String quiz_header,int quiz_id) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);
        AnsweringFragment answeringFragment = new AnsweringFragment();
        Bundle arguments = new Bundle();
        arguments.putString("quiz_header",quiz_header);
        arguments.putString("quiz_id",String.valueOf(quiz_id));
        arguments.putInt("user_points",points);
        arguments.putString("user_id",user_id);
        arguments.putString("name",name);
        arguments.putString("img_url",img_url);
        answeringFragment.setArguments(arguments);
        fragmentTransaction.replace(R.id.content_frame,answeringFragment,CURRENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public class NotificationTask extends AsyncTask<String, Void,String> {
        //        String notification_url = "http://nabadilika.dodoma-antinetal-pns.or.tz/wash/notifications.php";
        String notification_url = "http://192.168.43.125/wash/notifications_count.php";

        @Override
        protected String doInBackground(String... params) {

            String user_id = params[0];
            try {
                URL url = new URL(notification_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("user_id","UTF-8")+"="+URLEncoder.encode(user_id,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String response = "";
                String line;
                while ((line = bufferedReader.readLine())!=null)
                {
                    response+= line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("hii", "onPostExecute: "+result);
            if(result!=null){
                // showing dot next to notifications label
                navigationView.getMenu().getItem(1).setActionView(R.layout.menu_notification);
                View notification_num = navigationView.getMenu().getItem(1).getActionView();
                notification_count = notification_num.findViewById(R.id.notification_num);
                notification_count.setText(result);
            }
        }
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
