package com.example.apix.wash;

/**
 * Created by Apix on 10/09/2017.
 */

public class QuizCollection {
    String title,description,img_url,status;
    int id;

    public QuizCollection(int id,String title, String description, String img_url,String status) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.img_url = img_url;
        this.status = status;
    }
}
