package com.example.apix.wash;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


public class LeaderBoardFragment extends Fragment {

    LeaderBoardAdapter leaderBoardAdapter;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView recyclerView;
    LinearLayout oopsLayout;

    //    String url = "http://192.168.43.202/nabadilika/wash/leaderboard.php";
    private static String TAG = LeaderBoardFragment.class.getSimpleName();

    ArrayList<RankUser> users = new ArrayList<>();

    String user_id;

    public LeaderBoardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_leader_board, container, false);

        Bundle arguments = getArguments();
        user_id = arguments.getString("user_id");

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView = view.findViewById(R.id.recyclerView);
        oopsLayout = view.findViewById(R.id.oopsLayout);

        new LeaderBoardTask().execute(user_id);

        return view;
    }

    private class LeaderBoardTask extends AsyncTask<String, Void,String> {

//        SelectedPositionInterface selectInterface;
//
//        public LeaderBoardTask(SelectedPositionInterface selectInterface) {
//            this.selectInterface = selectInterface;
//        }
//        String scoreboard_url = "http://nabadilika.dodoma-antinetal-pns.or.tz/wash/leaderboard.php";
        String scoreboard_url = "http://192.168.43.125/wash/leaderboard.php";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String user_id = params[0];
            try {
                URL url = new URL(scoreboard_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("user_id","UTF-8")+"="+URLEncoder.encode(user_id,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String response = "";
                String line;
                while ((line = bufferedReader.readLine())!=null)
                {
                    response+= line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG, "doInBackground: "+result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("users");

                for(int i=0;i<jsonArray.length();i++){
                    JSONObject user = jsonArray.getJSONObject(i);

                    String name = user.getString("username");
                    int coins = Integer.parseInt(user.getString("coins"));
                    String user_id = user.getString("user_id");
                    String user_pos = user.getString("user_pos");

                    users.add(new RankUser(name,user_id,user_pos,coins));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (users.size()>0) {
                leaderBoardAdapter = new LeaderBoardAdapter(getContext(),users,user_id);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(leaderBoardAdapter);
            } else{
                recyclerView.setVisibility(View.GONE);
                oopsLayout.setVisibility(View.VISIBLE);
            }
        }
    }

}
