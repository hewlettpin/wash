package com.example.apix.wash;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.eugeneek.smilebar.SmileBar;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


/**
 * A simple {@link Fragment} subclass.
 */
public class CoinsFragment extends Fragment {

    RelativeLayout invite_friends,fb_like,feedback;
    RelativeLayout rate;
    TextView coins_text;
    ImageView feedback_img;
    String user_id;
    int user_points;

    public static String FACEBOOK_URL = "https://www.facebook.com/Nabadilika-udom-297982667370484";
    public static String FACEBOOK_PAGE_ID = "297982667370484";

    public CoinsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_coins, container, false);

        Bundle arguments = getArguments();
        user_points = arguments.getInt("user_points");
        user_id = arguments.getString("user_id");

        feedback_img = view.findViewById(R.id.feedback_img);

        coins_text = view.findViewById(R.id.coins_text);
        coins_text.setText("You Have "+String.format("%,d", user_points)+" Coins");

        feedback = view.findViewById(R.id.feedback);
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent feedbackIntent = new Intent(getContext(),FeedbackActivity.class);
                feedbackIntent.putExtra("user_id",user_id);
                feedbackIntent.putExtra("user_points",user_points);
                String transitionName = getContext().getString(R.string.transition_string);
                ActivityOptionsCompat options =
                        ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),feedback_img, transitionName);
                //Start the Intent
                ActivityCompat.startActivity(getActivity(), feedbackIntent, options.toBundle());
            }
        });

        invite_friends = view.findViewById(R.id.invite_friends);
        invite_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.apixgrounds.apix.egospel");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        fb_like = view.findViewById(R.id.fb_like);
        fb_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent facebookIntent = getOpenFacebookIntent(getContext());
                startActivity(facebookIntent);
                if(isOnline()){
                    new CoinsTask().execute("500",user_id);
                }else{
                    Toast.makeText(getContext(), "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        rate = view.findViewById(R.id.rate);
        rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog rankDialog = new Dialog(getContext(),R.style.FullHeightDialog);
                rankDialog.setContentView(R.layout.rating_dialog);
                rankDialog.setCancelable(true);
                SmileBar smileBar = rankDialog.findViewById(R.id.SmileBar);
                smileBar.setOnRatingSliderChangeListener(new SmileBar.OnRatingSliderChangeListener() {
                    @Override
                    public void onPendingRating(int rating) {
                        Log.i("onPendingRating", "" + rating);
                        // btn.setText("" + rating);
                    }

                    @Override
                    public void onFinalRating(int rating) {
                        Log.i("onFinalRating", "" + rating);
                    }

                    @Override
                    public void onCancelRating() {
                        Log.i("onCancelRating", "cancel");
                    }
                });
                smileBar.setRating(smileBar.getRating());


                TextView rateBtn = rankDialog.findViewById(R.id.rateBtn);
                rateBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rankDialog.dismiss();
                        if(isOnline()){
                            new CoinsTask().execute("250",user_id);
                        }else{
                            Toast.makeText(getContext(), "No Internet Connection!", Toast.LENGTH_SHORT).show();
                        }
                        Toast.makeText(getContext(),"Thanks for rating the App",Toast.LENGTH_SHORT).show();
                    }
                });
                rankDialog.show();
            }
        });



        return view;
    }

    public class CoinsTask extends AsyncTask<String, Void,String> {

//        private String feedback_url = "http://nabadilika.dodoma-antinetal-pns.or.tz/wash/coin_earn.php";
        private String feedback_url = "http://192.168.43.125/wash/coin_earn.php";

        @Override
        protected String doInBackground(String... params) {

            String coins = params[0];
            String user_id = params[1];
            try {
                URL url = new URL(feedback_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("coins","UTF-8")+"="+URLEncoder.encode(coins,"UTF-8")+"&"+
                        URLEncoder.encode("user_id","UTF-8")+"="+URLEncoder.encode(user_id,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String response = "";
                String line;
                while ((line = bufferedReader.readLine())!=null)
                {
                    response+= line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("hii", "onPostExecute: "+result);
            if (result.contains("Successful")) {
                String[] jibu = result.split("/");
                user_points = Integer.parseInt(jibu[1]);
                coins_text.setText("You Have "+String.format("%,d", user_points)+" Coins");
                Toast.makeText(getContext(), jibu[0], Toast.LENGTH_LONG).show();
            } else{
                Toast.makeText(getContext(), "Oops! Something Went Wrong..", Toast.LENGTH_LONG).show();
            }
        }
    }

    public static Intent getOpenFacebookIntent(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/"+FACEBOOK_PAGE_ID));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,Uri.parse(FACEBOOK_URL));
        }
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

}
