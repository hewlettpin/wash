package com.example.apix.wash;

/**
 * Created by Apix on 29/08/2017.
 */

public interface SelectedPositionInterface
{
    void onItemClick(AnswerAdapter.AnswerViewHolder holder, int correctness,String choice,int score);
}
