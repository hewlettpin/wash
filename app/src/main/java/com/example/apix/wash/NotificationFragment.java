package com.example.apix.wash;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {

    TextView notification_count;
    RecyclerView recyclerView;
    LinearLayout oopsLayout;
    RelativeLayout count_layout;
    NotificationAdapter notificationAdapter;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.ItemDecoration itemDecoration;

    String user_id;
    int read_counter = 0;

    ArrayList<Notification> notifications = new ArrayList<>();

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        Bundle arguments = getArguments();
        user_id = arguments.getString("user_id");

        notification_count = view.findViewById(R.id.notification_count);
        recyclerView = view.findViewById(R.id.recyclerView);
        oopsLayout = view.findViewById(R.id.oopsLayout);
        count_layout = view.findViewById(R.id.count_layout);
        layoutManager = new LinearLayoutManager(getContext());
        itemDecoration = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);

        new NotificationTask(getContext()).execute(user_id);

        return view;
    }

    public class NotificationTask extends AsyncTask<String, Void,String> {
        Context ctx;
        ProgressDialog dialog;
//        String notification_url = "http://nabadilika.dodoma-antinetal-pns.or.tz/wash/notifications.php";
        String notification_url = "http://192.168.43.125/wash/notifications.php";

        public NotificationTask(Context ctx) {
            this.ctx=ctx;
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ctx); // this = YourActivity
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Loading. Please wait...");
            dialog.setIndeterminate(true);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String user_id = params[0];
            try {
                URL url = new URL(notification_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("user_id","UTF-8")+"="+URLEncoder.encode(user_id,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String response = "";
                String line;
                while ((line = bufferedReader.readLine())!=null)
                {
                    response+= line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("hii", "onPostExecute: "+result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("notifications");

                for(int i=0;i<jsonArray.length();i++){
                    JSONObject user = jsonArray.getJSONObject(i);

                    String id = user.getString("id");
                    String type = user.getString("type");
                    String title = user.getString("title");
                    String body = user.getString("body");
                    String datetime = user.getString("datetime");
                    String status = user.getString("status");

                    if(status.equals("Yes")){
                        read_counter++;
                    }

                    notifications.add(new Notification(id,type,title,body,datetime,status));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (this.dialog != null) {
                this.dialog.dismiss();
            }
            if (notifications.size()>0) {
                notificationAdapter = new NotificationAdapter(notifications,getContext());
                count_layout.setVisibility(View.VISIBLE);
                notification_count.setText(String.valueOf(read_counter));
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.addItemDecoration(itemDecoration);
                recyclerView.setAdapter(notificationAdapter);
            } else{
                recyclerView.setVisibility(View.GONE);
                oopsLayout.setVisibility(View.VISIBLE);
            }
        }
    }

}
