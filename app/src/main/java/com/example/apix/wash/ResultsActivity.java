package com.example.apix.wash;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;

public class ResultsActivity extends AppCompatActivity {

    TextView congrats;
    TextView points;
    CardView quiz_btn;
    ImageView prof;

    boolean doubleBackToExitPressedOnce = false;

    RequestOptions requestOptions = new RequestOptions()
            .placeholder(R.drawable.prof)
            .error(R.drawable.ic_error)
            .fallback(R.drawable.prof);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_results);

        prof = findViewById(R.id.prof);
        congrats = findViewById(R.id.congrats);
        points = findViewById(R.id.points);
        quiz_btn = findViewById(R.id.quiz_btn);

        changeStatusBarColor();

        final String user_id = getIntent().getStringExtra("user_id");
        final String name = getIntent().getStringExtra("name");
        final String user_points = getIntent().getStringExtra("user_points");
        final String img_url = getIntent().getStringExtra("img_url");


        String result = getIntent().getStringExtra("result");
        String[] point = result.split(" ");
        int score = Integer.parseInt(point[0]);
        int total_score = Integer.parseInt(point[1]);

        if(img_url!=null){
            GlideApp.with(getApplicationContext()).load("http://192.168.43.125/wash/images/prof_images/"+img_url).apply(requestOptions).into(prof);
//            GlideApp.with(getApplicationContext()).load("http://nabadilika.dodoma-antinetal-pns.or.tz/wash/images/prof_images/"+img_url).apply(requestOptions).into(prof);
        }

        if(score<total_score/2){
            congrats.setText("Oops!");
        }else{
            congrats.setText("Congrats!");
        }

        points.setText("You got "+score+" scores..");

        quiz_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                intent.putExtra("user_id",user_id);
                intent.putExtra("name",name);
                intent.putExtra("coins",user_points);
                startActivity(intent);
            }
        });
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
