package com.example.apix.wash;


import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuizFragment extends Fragment {

    private FeatureCoverFlow mCoverFlow;
    private QuizCollectionAdapter mAdapter;
    private ArrayList<QuizCollection> mData = new ArrayList<>();
    String result_ans;

    MainInterface mainInterface;
    Activity mActivity;

//    private static String url = "http://nabadilika.dodoma-antinetal-pns.or.tz/wash/quiz_collection.php";
    private static String url = "http://192.168.43.125/wash/quiz_collection.php";
    private static String a_url = "http://192.168.43.125/wash/answered_ques.php";

    private static String TAG = QuizFragment.class.getSimpleName();

    String user_id;

    public QuizFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_quiz, container, false);

        mCoverFlow = view.findViewById(R.id.coverflow);

        Bundle arguments = getArguments();
        user_id = arguments.getString("user_id");

        new QuizCollectionTask().execute(user_id);

        mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if(isOnline()){
                    if(mData.get(position).status.equals("yes")){
                        Toast.makeText(getContext(), "Already played!", Toast.LENGTH_SHORT).show();
                    }else{
                        mainInterface.onClick(mData.get(position).title,mData.get(position).id);
                    }
//                }else{
//                    Toast.makeText(getContext(), "No Internet Connection!", Toast.LENGTH_SHORT).show();
//                }
            }
        });

        mCoverFlow.setOnScrollPositionListener(new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {

            }

            @Override
            public void onScrolling() {

            }
        });

        return view;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        mainInterface = (MainInterface) mActivity;
    }

    interface MainInterface{
        void onClick(String quiz_header,int quiz_id);
    }

    private class QuizCollectionTask extends AsyncTask<String, Void,Void>{

        @Override
        protected Void doInBackground(String... params) {

            String user_id = params[0];
            try {
                URL url = new URL(a_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("user_id","UTF-8")+"="+URLEncoder.encode(user_id,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String response = "";
                String line;
                while ((line = bufferedReader.readLine())!=null)
                {
                    response+= line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                result_ans = response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

            String[] ans_questions = result_ans.split(" ");
            HttpHandler httpHandler = new HttpHandler();
            String result = httpHandler.makeServiceCall(url);
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("quizes");

                Log.d(TAG, "doInBackground: "+jsonArray);

                for(int i=0;i<jsonArray.length();i++){
                    JSONObject quizObject = jsonArray.getJSONObject(i);

                    int quiz_id = quizObject.getInt("quiz_id");
                    String quiz_name = quizObject.getString("quiz_name");
                    String quiz_desc = quizObject.getString("quiz_descr");
                    String quiz_img = quizObject.getString("quiz_img");
                    String status = "no";
                    for(int j=0;j<ans_questions.length;j++){
                        if(ans_questions[j].equals(String.valueOf(quiz_id))){
                            status="yes";
                        }
                    }
                    mData.add(new QuizCollection(quiz_id,quiz_name,quiz_desc,quiz_img,status));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mCoverFlow.setVisibility(View.VISIBLE);
            mAdapter = new QuizCollectionAdapter(getContext(),mData,result_ans);
            mCoverFlow.setAdapter(mAdapter);
        }
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

}
