package com.example.apix.wash;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

/**
 * Created by Apix on 09/10/2017.
 */

public class LeaderBoardAdapter extends RecyclerView.Adapter<LeaderBoardAdapter.LeaderViewHolder>{

    ArrayList<RankUser> users;
    Context context;
    String user_id;

    private RequestOptions requestOptions = new RequestOptions()
            .placeholder(R.drawable.prof)
            .error(R.drawable.ic_error)
            .fallback(R.drawable.prof);

    public LeaderBoardAdapter(Context context,ArrayList<RankUser> users,String user_id) {
        this.users = users;
        this.context = context;
        this.user_id = user_id;
    }

    public class LeaderViewHolder extends RecyclerView.ViewHolder{

        TextView position,name,coins;
        ImageView prof_img,crown;
        RelativeLayout rank_background,num_layout;

        public LeaderViewHolder(View itemView) {
            super(itemView);
            position = itemView.findViewById(R.id.position);
            name = itemView.findViewById(R.id.name);
            coins = itemView.findViewById(R.id.coins);
            prof_img = itemView.findViewById(R.id.prof_img);
            crown = itemView.findViewById(R.id.crown);
            rank_background = itemView.findViewById(R.id.rank_background);
            num_layout = itemView.findViewById(R.id.num_layout);
        }
    }

    @Override
    public LeaderBoardAdapter.LeaderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rank_layout,null);
        return new LeaderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LeaderBoardAdapter.LeaderViewHolder holder, int position) {
        holder.position.setText(users.get(position).position);
        holder.name.setText(users.get(position).name);
        holder.coins.setText(String.format("%,d", users.get(position).coins)+" Coins");
//        GlideApp.with(context).load("http://nabadilika.dodoma-antinetal-pns.or.tz/wash/images/prof_images/"+"prof.png").apply(requestOptions).into(holder.prof_img);
        GlideApp.with(context).load("http://192.168.43.125/wash/images/prof_images/"+"prof.png").apply(requestOptions).into(holder.prof_img);
        if(position==0){
            holder.crown.setVisibility(View.VISIBLE);
        }
        if(users.get(position).user_id.equals(user_id)){
            holder.rank_background.setBackgroundResource(R.drawable.shape4);
        }
        if(Integer.parseInt(users.get(position).position)>10){
            final int color = ContextCompat.getColor(context, R.color.guillotine_background);
            holder.num_layout.setBackgroundColor(color);
        }
    }

    public int getItemCount() {
        return users.size();
    }
}
