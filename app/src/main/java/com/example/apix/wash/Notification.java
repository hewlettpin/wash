package com.example.apix.wash;

/**
 * Created by Apix on 02/12/2017.
 */

public class Notification {
    String id,type,title,body,time,status;

    public Notification(String id,String type, String title, String body, String time, String status) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.body = body;
        this.time = time;
        this.status = status;
    }
}
