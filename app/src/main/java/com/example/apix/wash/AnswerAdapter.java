package com.example.apix.wash;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;


/**
 * Created by Apix on 28/08/2017.
 */

public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.AnswerViewHolder> {

    List<Answer> answers;
    Context context;
    int selectedPosition = -1;
    String choice = "yes";

    private SelectedPositionInterface interfaceclick;

    public AnswerAdapter(Context context, SelectedPositionInterface interfaceclick, List<Answer> answers) {
        super();
        this.answers = answers;
        this.context = context;
        this.interfaceclick = interfaceclick;
    }

    public class AnswerViewHolder extends RecyclerView.ViewHolder{

        TextView answer;
        AppCompatRadioButton radioButton;
        RelativeLayout answer_view;

        public AnswerViewHolder(View itemView) {
            super(itemView);
            answer = itemView.findViewById(R.id.answer);
            radioButton = itemView.findViewById(R.id.radio_btn);
            answer_view = itemView.findViewById(R.id.answer_view);

            ColorStateList colorStateList = new ColorStateList(
                    new int[][]{
                            new int[]{-android.R.attr.state_checked},
                            new int[]{android.R.attr.state_checked}
                    },
                    new int[]{

                            Color.DKGRAY
                            , context.getResources().getColor(R.color.radio_btn_color),
                    }
            );
            radioButton.setSupportButtonTintList(colorStateList);
        }
    }

    public AnswerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.answer_layout,null);
        return new AnswerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AnswerViewHolder holder, final int position) {
        holder.answer.setText(answers.get(position).answer);
        holder.radioButton.setChecked(position == selectedPosition);
        holder.radioButton.setTag(position);
        holder.answer.setTag(position);
        holder.answer_view.setTag(position);
        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCheckChanged(v);
                interfaceclick.onItemClick(holder, answers.get(position).is_corrent,choice,answers.get(position).score);
            }
        });
        holder.answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCheckChanged(v);
                interfaceclick.onItemClick(holder, answers.get(position).is_corrent,choice,answers.get(position).score);
            }
        });
        holder.answer_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCheckChanged(v);
                interfaceclick.onItemClick(holder, answers.get(position).is_corrent,choice,answers.get(position).score);
            }
        });
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    //Return the selectedPosition item
    public int getSelectedItem() {
        if (selectedPosition != -1) {
//            Toast.makeText(context, "Selected Item : " + selectedPosition, Toast.LENGTH_SHORT).show();
            return selectedPosition;
        }
        return -1;
    }

    public void clear() {
        answers.clear(); //clear list
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return answers.size();
    }


}
