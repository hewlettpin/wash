package com.example.apix.wash;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appolica.flubber.Flubber;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.util.Date;

public class WelcomeActivity extends AppCompatActivity {

//    String login_url = "http://nabadilika.dodoma-antinetal-pns.or.tz/wash/app_login.php";
    String login_url = "http://192.168.43.125/wash/app_login.php";
//    String registration_url = "http://nabadilika.dodoma-antinetal-pns.or.tz/wash/register_app.php";
    String registration_url = "http://192.168.43.125/wash/register_app.php";

    EditText full_name, username,password, phone,username_log,password_log;
    TextInputLayout username_layout,email_layout,password_layout,confirm_layout,username_login,password_login;
    CardView signup,login,signUp,Login,next;
    TextView forgot,lowerTxt;
    RelativeLayout signRelative,about_view;
    DatePicker dob;
    TextView date_txt;

    Handler handler = new Handler();
    Handler mhandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        lowerTxt = findViewById(R.id.lowerTxt);

        username_layout = findViewById(R.id.username_layout);
        email_layout = findViewById(R.id.email_layout);
        password_layout = findViewById(R.id.password_layout);
        confirm_layout = findViewById(R.id.confirm_layout);

        full_name = findViewById(R.id.full_name);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        phone = findViewById(R.id.phone);
        dob = findViewById(R.id.dob);
        signUp = findViewById(R.id.signUp);
        next = findViewById(R.id.next);
        date_txt = findViewById(R.id.date_txt);

        signup = findViewById(R.id.signup);
        login = findViewById(R.id.login);

        username_login = findViewById(R.id.username_login);
        password_login = findViewById(R.id.password_login);
        Login = findViewById(R.id.Login);
        forgot = findViewById(R.id.forgot);
        signRelative = findViewById(R.id.signRelative);
        about_view = findViewById(R.id.about_view);

        username_log = findViewById(R.id.username_log);
        password_log = findViewById(R.id.password_log);

        username_layout.setTranslationX(1000f);
        email_layout.setTranslationX(1000f);
        password_layout.setTranslationX(1000f);
        confirm_layout.setTranslationX(1000f);
        signUp.setTranslationX(1000f);
        next.setTranslationX(1000f);
        date_txt.setVisibility(View.INVISIBLE);

        username_login.setTranslationX(-1000f);
        password_login.setTranslationX(-1000f);
        Login.setTranslationX(-1000f);
        forgot.setTranslationX(-1000f);

        signRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lowerClick();
            }
        });

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = username_log.getText().toString();
                String password = password_log.getText().toString();
                if(email.isEmpty()||password.isEmpty()){
                    Toast.makeText(WelcomeActivity.this, "Please Enter All Login Credentials..", Toast.LENGTH_SHORT).show();
                }else{
//                    if(isOnline()){
                        LoginTask loginTask = new LoginTask(WelcomeActivity.this);
                        loginTask.execute(email, password);
//                    }else{
//                        Toast.makeText(WelcomeActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
//                    }
                }
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String fullname = full_name.getText().toString();
                final String username = WelcomeActivity.this.username.getText().toString();
                final String phone = WelcomeActivity.this.phone.getText().toString();
                final String org_password = password.getText().toString();
                final String date = dob.getYear()+"/"+dob.getMonth()+"/"+dob.getDayOfMonth();
                Toast.makeText(WelcomeActivity.this, date, Toast.LENGTH_SHORT).show();
                if(isOnline()){
                    new android.support.v7.app.AlertDialog.Builder(WelcomeActivity.this)
                            .setTitle("Newsletter Subscription")
                            .setMessage("Would you like to receive notifications through your username?")
                            .setIcon(R.drawable.info)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    RegistrationTask registrationTask = new RegistrationTask(WelcomeActivity.this);
                                    registrationTask.execute(fullname,username,org_password,phone,date,"on");
                                    lowerClick();
                                    signRelative.setVisibility(View.INVISIBLE);
                                    //finish();
                                }})
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    RegistrationTask registrationTask = new RegistrationTask(WelcomeActivity.this);
                                    registrationTask.execute(fullname,username,org_password,phone,date,"off");
                                    lowerClick();
                                    signRelative.setVisibility(View.INVISIBLE);
                                }
                            }).show();
                }else {
                    Toast.makeText(WelcomeActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String fullname = full_name.getText().toString();
                final String username = WelcomeActivity.this.username.getText().toString();
                final String phone = WelcomeActivity.this.phone.getText().toString();
                final String org_password = password.getText().toString();
                if(fullname.isEmpty()||username.isEmpty()||phone.isEmpty()||org_password.isEmpty()){
                    Toast.makeText(WelcomeActivity.this, "Please fill all fields!", Toast.LENGTH_SHORT).show();
                }else{
                    username_layout.setVisibility(View.INVISIBLE);
                    email_layout.setVisibility(View.INVISIBLE);
                    password_layout.setVisibility(View.INVISIBLE);
                    confirm_layout.setVisibility(View.INVISIBLE);
                    dob.setVisibility(View.VISIBLE);
                    signUp.setVisibility(View.VISIBLE);
                    next.setVisibility(View.INVISIBLE);
                    date_txt.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    public void signUp(View view){
        about_view.setVisibility(View.INVISIBLE);
        full_name.requestFocus();

        lowerTxt.setText("Login");

        username_layout.animate().translationXBy(-1000f).setDuration(250);
        email_layout.animate().translationXBy(-1000f).setDuration(350);
        password_layout.animate().translationXBy(-1000f).setDuration(450);
        confirm_layout.animate().translationXBy(-1000f).setDuration(550);
        signUp.animate().translationXBy(-1000f).setDuration(650);
        next.animate().translationXBy(-1000f).setDuration(650);

        signup.animate().translationXBy(1000f).setDuration(250);
        login.animate().translationXBy(1000f).setDuration(250);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Flubber.with()
                        .animation(Flubber.AnimationPreset.SHAKE) // Slide up animation
                        .repeatCount(0)                              // Repeat once
                        .duration(550)                              // Last for 1000 milliseconds(1 second)
                        .createFor(username_layout)                             // Apply it to the view
                        .start();
                signRelative.setVisibility(View.VISIBLE);
            }
        },150);
    }

    public void login(View view){
        about_view.setVisibility(View.INVISIBLE);
        username_log.requestFocus();

        username_login.animate().translationXBy(1000f).setDuration(250);
        password_login.animate().translationXBy(1000f).setDuration(400);
        Login.animate().translationXBy(1000f).setDuration(500);
        forgot.animate().translationXBy(1000f).setDuration(600);

        signup.animate().translationXBy(-1000f).setDuration(250);
        login.animate().translationXBy(-1000f).setDuration(250);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Flubber.with()
                        .animation(Flubber.AnimationPreset.SHAKE) // Slide up animation
                        .repeatCount(0)                              // Repeat once
                        .duration(550)                              // Last for 1000 milliseconds(1 second)
                        .createFor(username_login)                             // Apply it to the view
                        .start();
                signRelative.setVisibility(View.VISIBLE);
            }
        },150);
    }

    public void lowerClick(){
        if(lowerTxt.getText().toString().equals("Login")){
            username_layout.animate().translationXBy(1000f).setDuration(250);
            email_layout.animate().translationXBy(1000f).setDuration(350);
            password_layout.animate().translationXBy(1000f).setDuration(450);
            confirm_layout.animate().translationXBy(1000f).setDuration(550);
            signUp.animate().translationXBy(1000f).setDuration(650);
            next.animate().translationXBy(1000f).setDuration(650);
            date_txt.setVisibility(View.INVISIBLE);
            dob.setVisibility(View.INVISIBLE);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    username_log.requestFocus();

                    username_login.animate().translationXBy(1000f).setDuration(250);
                    password_login.animate().translationXBy(1000f).setDuration(400);
                    Login.animate().translationXBy(1000f).setDuration(500);
                    forgot.animate().translationXBy(1000f).setDuration(600);

                    lowerTxt.setText("Sign Up");

                    mhandler.postDelayed(new Runnable(){
                        @Override
                        public void run() {
                            Flubber.with()
                                    .animation(Flubber.AnimationPreset.SHAKE) // Slide up animation
                                    .repeatCount(0)                              // Repeat once
                                    .duration(550)                              // Last for 1000 milliseconds(1 second)
                                    .createFor(username_login)                             // Apply it to the view
                                    .start();
                        }
                    },150);
                }
            },200);
        }else if(lowerTxt.getText().toString().equals("Sign Up")){
            full_name.requestFocus();

            username_login.animate().translationXBy(-1000f).setDuration(250);
            password_login.animate().translationXBy(-1000f).setDuration(400);
            Login.animate().translationXBy(-1000f).setDuration(500);
            forgot.animate().translationXBy(-1000f).setDuration(600);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    username_layout.animate().translationXBy(-1000f).setDuration(250);
                    email_layout.animate().translationXBy(-1000f).setDuration(350);
                    password_layout.animate().translationXBy(-1000f).setDuration(450);
                    confirm_layout.animate().translationXBy(-1000f).setDuration(550);
                    signUp.animate().translationXBy(-1000f).setDuration(650);
                    next.animate().translationXBy(-1000f).setDuration(650);

                    lowerTxt.setText("Login");

                    mhandler.postDelayed(new Runnable(){
                        @Override
                        public void run() {
                            Flubber.with()
                                    .animation(Flubber.AnimationPreset.SHAKE) // Slide up animation
                                    .repeatCount(0)                              // Repeat once
                                    .duration(550)                              // Last for 1000 milliseconds(1 second)
                                    .createFor(username_layout)                             // Apply it to the view
                                    .start();
                        }
                    },150);
                }
            },200);
        }
    }

    public class LoginTask extends AsyncTask<String, Void,String> {
        Context ctx;
        ProgressDialog dialog;

        public LoginTask(Context ctx) {
            this.ctx=ctx;
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ctx); // this = YourActivity
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Loading. Please wait...");
            dialog.setIndeterminate(true);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

                String login_name = params[0];
                String login_pass = params[1];
                try {
                    URL url = new URL(login_url);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                    String data = URLEncoder.encode("username","UTF-8")+"="+URLEncoder.encode(login_name,"UTF-8")+"&"+
                            URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(login_pass,"UTF-8");
                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                    String response = "";
                    String line;
                    while ((line = bufferedReader.readLine())!=null)
                    {
                        response+= line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    return response;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();

                }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("hii", "onPostExecute: "+result);
            if (result.contains("Successful")) {
                String[] userDetails = result.split("/");
                Intent intent = new Intent(ctx, MainActivity.class);
                intent.putExtra("name",userDetails[1]);
                intent.putExtra("coins",userDetails[2]);
                intent.putExtra("img_url",userDetails[3]);
                intent.putExtra("user_id",userDetails[4]);
                startActivity(intent);
                finish();

            } else{
                if (this.dialog != null) {
                    this.dialog.dismiss();
                }
                Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();
            }
        }
    }


    public class RegistrationTask extends AsyncTask<String, Void,String> {
        Context ctx;
        ProgressDialog dialog;

        public RegistrationTask(Context ctx) {
            this.ctx=ctx;
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ctx); // this = YourActivity
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Loading. Please wait...");
            dialog.setIndeterminate(true);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String full_name = params[0];
            String email = params[1];
            String password = params[2];
            String phone = params[3];
            String dob = params[4];
            String newsletter = params[5];

            try {
                URL url = new URL(registration_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("fullname","UTF-8")+"="+URLEncoder.encode(full_name,"UTF-8")+"&"+
                        URLEncoder.encode("username","UTF-8")+"="+URLEncoder.encode(email,"UTF-8")+"&"+
                        URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(password,"UTF-8")+"&"+
                        URLEncoder.encode("phone","UTF-8")+"="+URLEncoder.encode(phone,"UTF-8")+"&"+
                        URLEncoder.encode("dob","UTF-8")+"="+URLEncoder.encode(dob,"UTF-8")+"&"+
                        URLEncoder.encode("newsletter","UTF-8")+"="+URLEncoder.encode(newsletter,"UTF-8")+"&";
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String response = "";
                String line;
                while ((line = bufferedReader.readLine())!=null)
                {
                    response+= line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            if (result.contains("Successful")) {
                Log.d("hii", "onPostExecute: "+result);
//                String[] userDetails = result.split("/");
//
//                Intent intent = new Intent(ctx, MainActivity.class);
//                intent.putExtra("name",userDetails[0]);
//                intent.putExtra("points",userDetails[1]);
//                intent.putExtra("user_id",userDetails[2]);
//                startActivity(intent);
//                finish();
                Toast.makeText(ctx, "Karibu Nabadilika App.. Ingia kuendelea!", Toast.LENGTH_LONG).show();

            } else if(result.contains("Failed")) {
                Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();
            }
        }
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
}
