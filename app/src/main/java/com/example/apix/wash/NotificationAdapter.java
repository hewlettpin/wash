package com.example.apix.wash;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Apix on 02/12/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    ArrayList<Notification> notifications;
    Context context;
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date now = new Date();

    public NotificationAdapter(ArrayList<Notification> notifications, Context context) {
        this.notifications = notifications;
        this.context = context;
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView,status_img;
        TextView title,body,time;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            body = itemView.findViewById(R.id.body);
            time = itemView.findViewById(R.id.time);
            status_img = itemView.findViewById(R.id.status_img);
        }
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_layout,null);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        if(notifications.get(position).type.equals("prize")){
            holder.imageView.setImageResource(R.drawable.coin1);
        }else if(notifications.get(position).type.equals("info")){
            holder.imageView.setImageResource(R.drawable.info);
        }
        holder.title.setText(notifications.get(position).title);
        String subDesc = notifications.get(position).body;
        if (subDesc.length() > 25) {
            subDesc = subDesc.substring(0, 25);
            holder.body.setText(subDesc);
            holder.body.append("...");
        } else
            holder.body.setText(subDesc);
        try
        {
            Date past = format.parse(notifications.get(position).time);
            long seconds=TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes=TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours=TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days= TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if(seconds<60)
            {
               holder.time.setText(seconds+" seconds ago");
            }
            else if(minutes<60)
            {
                holder.time.setText(minutes+" minutes ago");
            }
            else if(hours<24)
            {
                holder.time.setText(hours+" hours ago");
            }
            else
            {
                holder.time.setText(days+" days ago");
            }
        }
        catch (Exception j){
            j.printStackTrace();
        }
        if(notifications.get(position).status.equals("No")){
            holder.status_img.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }
}
