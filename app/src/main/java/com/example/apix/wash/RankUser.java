package com.example.apix.wash;

/**
 * Created by Apix on 09/10/2017.
 */

public class RankUser {
    String name,user_id,position;
    int coins;

    public RankUser(String name,String user_id,String position, int coins) {
        this.name = name;
        this.user_id = user_id;
        this.position = position;
        this.coins = coins;
    }
}
